package ie.ucd.hellospringboot;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloController {
    int count = 0;

    @GetMapping("/counter")
    public String counter() {
        String page = (count++ % 2 == 0) ? "even.html":"odd.html";
        System.out.println("count: " + count + " / page: " + page);
        return page;        
    }
}